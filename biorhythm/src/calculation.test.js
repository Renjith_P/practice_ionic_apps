import { calculateBiorhythms } from "./calculations";

it ('Calculate the physical Biorhythm',() => {
    const {physical} = calculateBiorhythms('1997-08-14','2021-08-12')
    expect(physical).toBeCloseTo(0.2698)
});

it ('Calculate the physical Biorhythm',() => {
    const {emotional} = calculateBiorhythms('1997-08-14','2021-08-12')
    expect(emotional).toBeCloseTo(0.0000)
});

it ('Calculate the physical Biorhythm',() => {
    const {intellectual} = calculateBiorhythms('1997-08-14','2021-08-12')
    expect(intellectual).toBeCloseTo(-0.4582)
});