import React , { useState }from 'react';
import { 
  IonButton, 
  IonIcon, 
  IonToast, 
  IonToolbar,
  IonApp,
  IonHeader,
  IonTitle,
  IonContent
} from '@ionic/react'
import { handRightOutline as handIcon } from 'ionicons/icons';

function App() {
  const [showToast, setShowToast] = useState(false);
  const handleClick = () => {
    setShowToast(true);
    setTimeout(()=>setShowToast(false), 1500);
  }
  return (
    <IonApp>
      <IonHeader>
        <IonToolbar>
          <IonTitle>My App</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className = "ion-padding">
        <IonButton onClick ={ handleClick }>
          <IonIcon icon={handIcon} slot="start"/>
          Click me
        </IonButton>
        <IonToast isOpen = {showToast} message = "Hello World!!" />
      </IonContent>
    </IonApp>
  );
}

export default App;
